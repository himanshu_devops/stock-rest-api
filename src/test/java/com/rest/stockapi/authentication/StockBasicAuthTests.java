package com.rest.stockapi.authentication;

import com.rest.stockapi.authentication.StockBasicAuth;
import com.rest.stockapi.controllers.StockApiController;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(value = StockBasicAuth.class, secure = false)
@AutoConfigureMockMvc
public class StockBasicAuthTests {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private StockApiController controller;

    @MockBean
    private StockBasicAuthEntryPoint authEntryPoint;

    @WithMockUser(username = "admin")
    @Test
    public void givenAuthRequestService_shouldSucceedWith200() throws Exception {
        mvc.perform(get("/stocks").contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());
    }

}
