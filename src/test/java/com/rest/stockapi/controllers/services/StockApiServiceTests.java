package com.rest.stockapi.controllers.services;

import com.rest.stockapi.exceptions.StockApiException;
import com.rest.stockapi.models.Stock;
import com.rest.stockapi.repositories.StockRepository;
import com.rest.stockapi.service.StockApiServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;
import static org.mockito.Mockito.*;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
public class StockApiServiceTests {

    @Mock
    private StockRepository stockRepository;

    @InjectMocks
    private StockApiServiceImpl stockApiService;

    private List<Stock> stockList;
    private Stock stock;

    @Test
    public void TestGetAllStocks() {

        Mockito.when(stockRepository.findAll()).thenReturn(stockList);
        List<Stock> stockList = stockApiService.getAllStocks();
        assertEquals(3, stockList.size());
        assertEquals("SunPharma", stockList.get(0).getStockName());
        assertEquals("CapitalMarket", stockList.get(1).getStockName());
        assertEquals(100, stockList.get(0).getCurrentPrice().intValue());
        assertEquals(200, stockList.get(1).getCurrentPrice().intValue());
    }

    @Test
    public void TestAddStock()
    {
        stockApiService.addStock(stock);
        verify(stockRepository).save(any(Stock.class));

    }

    @Test
    public void TestGetStockById() {

        Mockito.when(stockRepository.findById(anyLong())).thenReturn(Optional.of(stock));
        Stock stock = stockApiService.getStockById(1L);
        assertEquals("SunPharma", stock.getStockName());
        assertEquals(200, stock.getCurrentPrice().intValue());
    }

    @Test(expected = StockApiException.class)
    public void TestGetStockByIdNotFound() {

        Mockito.when(stockRepository.findById(anyLong())).thenReturn(Optional.empty());
        stockApiService.getStockById(1L);
    }

    @Test
    public void TestUpdateStock() {

        Mockito.when(stockRepository.findById(anyLong())).thenReturn(Optional.of(new Stock(stock)));
        stockApiService.updateStock(1L, stock);
        verify(stockRepository).save(any(Stock.class));
    }

    @Test
    public void TestUpdateStockNotFound() {

        Mockito.when(stockRepository.findById(anyLong())).thenReturn(Optional.empty());
        try {
            stockApiService.updateStock(1L, stock);
            fail();
        } catch (StockApiException e) {
            verify(stockRepository, never()).save(any(Stock.class));
        }
    }

    @Before
    public void prepareData()
    {
        stockList= Arrays.asList(new Stock("SunPharma", new BigDecimal(100)),
            new Stock("CapitalMarket", BigDecimal.valueOf(200)),
            new Stock("PayTM", BigDecimal.valueOf(50)));
        stock=new Stock("SunPharma", BigDecimal.valueOf(200));

    }
}
