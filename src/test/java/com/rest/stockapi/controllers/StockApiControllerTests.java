package com.rest.stockapi.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rest.stockapi.exceptions.StockApiException;
import com.rest.stockapi.models.Stock;
import com.rest.stockapi.repositories.StockRepository;
import com.rest.stockapi.service.StockApiServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(value = StockApiController.class, secure = false)
public class StockApiControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private StockApiServiceImpl stockApiService;

    @MockBean
    private StockRepository stockRepository;

    private List<Stock> stockList;
    private Stock stock;
    private static ObjectMapper objectMapper;

    @Test
    public void TestAddStock() throws Exception {

        this.mockMvc.perform(post("/addStock")
            .contentType(MediaType.APPLICATION_JSON)
            .characterEncoding("utf-8")
            .content(objectMapper.writeValueAsString(stock)))
            .andExpect(status().isOk());
    }

    @Test
    public void testGetAllStocks() throws Exception {

        Mockito.when(stockApiService.getAllStocks()).thenReturn(stockList);
        this.mockMvc.perform(get("/stocks")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", hasSize(3)))
            .andExpect(jsonPath("$[0].stockName", equalTo(stockList.get(0).getStockName())))
            .andExpect(jsonPath("$[0].currentPrice", is(stockList.get(0).getCurrentPrice().intValue())));
    }

    @Test
    public void TestGetStockById() throws Exception {

        Mockito.when(stockApiService.getStockById(1L)).thenReturn(stock);
        this.mockMvc.perform(get("/getStock/1")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.*", hasSize(4)))
            .andExpect(jsonPath("$.stockName", equalTo(stock.getStockName())))
            .andExpect(jsonPath("$.currentPrice", is(stock.getCurrentPrice().intValue())));
    }

    @Test
    public void TestGetStockByIdNotFound() throws Exception {

        Mockito.when(stockApiService.getStockById(1L)).thenThrow(new StockApiException("No Stock found"));
        this.mockMvc.perform(get("/stocks/1")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isNotFound());
    }

    @Test
    public void TestUpdateStock() throws Exception {

        Mockito.doThrow(new StockApiException("No stock found")).when(stockApiService).updateStock(1L, stock);
        this.mockMvc.perform(put("/updateStock/1")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isBadRequest());
    }

    @Test
    public void TestUpdateStockNotFound() throws Exception {
        this.mockMvc.perform(put("/updateStock/1")
            .contentType(MediaType.APPLICATION_JSON)
            .characterEncoding("utf-8")
            .content(objectMapper.writeValueAsString(stock)))
            .andExpect(status().isOk());
    }

    @Before
    public void prepareData()
    {
        stockList= Arrays.asList(new Stock("SunPharma", new BigDecimal(100)),
            new Stock("CapitalMarket", BigDecimal.valueOf(200)),
            new Stock("PayTM", BigDecimal.valueOf(50)));
        stock=new Stock("SunPharma", BigDecimal.valueOf(200));
        objectMapper = new ObjectMapper();
    }

}
