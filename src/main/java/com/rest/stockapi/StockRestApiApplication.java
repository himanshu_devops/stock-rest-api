package com.rest.stockapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StockRestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(StockRestApiApplication.class, args);
	}

}
