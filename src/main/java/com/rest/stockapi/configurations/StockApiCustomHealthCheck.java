package com.rest.stockapi.configurations;

import com.rest.stockapi.utils.StockApiConstants;
import org.springframework.boot.actuate.health.AbstractHealthIndicator;
import org.springframework.boot.actuate.health.Health;
import org.springframework.stereotype.Component;

/**
 * Class for Customizing the health check of stock API
 * @Author Himanshu Tyagi
 */

@Component
public class StockApiCustomHealthCheck extends AbstractHealthIndicator {

    /**
     * Customize the health check of stock API
     * @param builder
     * @throws Exception
     */

    @Override
    protected void doHealthCheck(Health.Builder builder) throws Exception {
        builder.up()
            .withDetail(StockApiConstants.APP, StockApiConstants.HEALTH_CHECK_APP)
            .withDetail(StockApiConstants.ERROR, StockApiConstants.HEALTH_CHECK_ERROR);
    }
}
