package com.rest.stockapi.configurations;

import com.rest.stockapi.models.Stock;
import com.rest.stockapi.repositories.StockRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import java.math.BigDecimal;

/**
 * Class for loading the initial data in DB
 * @Author Himanshu Tyagi
 */

@Profile("local")
@Component
public class LoadAtStartUp implements CommandLineRunner{

    private static final Logger logger = LoggerFactory.getLogger(LoadAtStartUp.class);

    @Autowired
    private StockRepository stockRepository;

    /**
     * Loads initial data
     * @param args
     * @throws Exception
     */

    @Override
    public void run(String... args) throws Exception {
        logger.info("Loading list of stocks in memory at start up");
        stockRepository.save(new Stock("ING", BigDecimal.valueOf(10.43)));
        stockRepository.save(new Stock("Payconiq", BigDecimal.valueOf(10.01)));
        stockRepository.save(new Stock("MasterCard", BigDecimal.valueOf(269.13)));
        stockRepository.save(new Stock("Paypal", BigDecimal.valueOf(102.20)));
    }
}
