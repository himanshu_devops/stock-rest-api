package com.rest.stockapi.utils;

import com.rest.stockapi.exceptions.StockApiException;

/**
 * Constants class
 * @Author Himanshu Tyagi
 */


public class StockApiConstants {
    private StockApiConstants() {
        throw new StockApiException(StockApiConstants.STKAPI002);
    }

    public static final String APP = "app";
    public static final String ERROR = "error";
    public static final String REALM_NAME = "STOCK-API";
    public static final String HTTP_STATUS = "HTTP Status 401 - ";
    public static final String AUTHENTICATE = "WWW-Authenticate";
    public static final String BASIC_REALM= "Basic realm=";
    public static final String HEALTH_CHECK_APP= "Application is in active mode";
    public static final String HEALTH_CHECK_ERROR= "No error reported so far";
    public static final String STKAPI001 = "Stock id %s could not be located in database";
    public static final String STKAPI002 = "StockApiConstants Exception";
    public static final String LIST_STOCK = "Returned list of Stocks from the operation 'getAllStocks'";
    public static final String STOCK_ID = "Stock supplied for the operation 'getStockById' with id: {}";
    public static final String STOCK = "Stock supplied for the operation 'addStock' with value: {}";
    public static final String STOCK_AND_ID = "Id and stock supplied for the operation 'updateStock' are : {},{}";
    public static final String UPDATED_DETAILS = "Updated details are : {},{}";
}
