package com.rest.stockapi.authentication;

import com.rest.stockapi.utils.StockApiConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Authentication enrty point class
 * @Author Himanshu Tyagi
 */

@Component
public class StockBasicAuthEntryPoint extends BasicAuthenticationEntryPoint {

    private static final Logger logger = LoggerFactory.getLogger(StockBasicAuthEntryPoint.class);

    /**
     * Override the basic authentication entry point
     * @param request
     * @param response
     * @param authEx
     * @throws IOException
     * @throws ServletException
     */

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authEx)
        throws IOException, ServletException {
        response.addHeader(StockApiConstants.AUTHENTICATE, StockApiConstants.BASIC_REALM + getRealmName());
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        logger.info(StockApiConstants.HTTP_STATUS + authEx.getMessage());
    }

    /**
     * Set the properties
     * @throws Exception
     */
    @Override
    public void afterPropertiesSet() throws Exception {
        setRealmName(StockApiConstants.REALM_NAME);
        super.afterPropertiesSet();
    }
}
