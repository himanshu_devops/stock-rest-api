package com.rest.stockapi.models;

import lombok.NoArgsConstructor;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * Model class Stock
 * @Author Himanshu Tyagi
 */


@NoArgsConstructor
@Entity
@Table(name="STOCK")
public class Stock {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long stockId;

    @Column(name="STOCK_NAME")
    private String stockName;

    @Column(name="CURRENT_PRICE")
    private BigDecimal currentPrice;

    @UpdateTimestamp
    @Column(name="LAST_UPDATED")
    private LocalDateTime lastUpdated;

    public Stock(String stockName, BigDecimal currentPrice) {
        this.stockName = stockName;
        this.currentPrice = currentPrice;
    }

    public Stock(Stock stock) {
        this.stockId=stock.getStockId();
        this.stockName = stock.getStockName();
        this.currentPrice = stock.getCurrentPrice();
        this.lastUpdated=stock.getLastUpdated();
    }

    public Long getStockId() {
        return stockId;
    }

    public String getStockName() {
        return stockName;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    public BigDecimal getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(BigDecimal currentPrice) {
        this.currentPrice = currentPrice;
    }

    public LocalDateTime getLastUpdated() {
        return lastUpdated;
    }
}
