package com.rest.stockapi.repositories;

import com.rest.stockapi.models.Stock;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Stock data repository
 * @Author Himanshu Tyagi
 */

public interface StockRepository extends JpaRepository<Stock, Long> {
}
