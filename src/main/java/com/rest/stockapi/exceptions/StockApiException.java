package com.rest.stockapi.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception class for Stock API
 * @Author Himanshu Tyagi
 */

@ResponseStatus(HttpStatus.NOT_FOUND)
public class StockApiException extends RuntimeException{

    /**
     * Stock API exception class constructor
     * @param expMessage
     */
    public StockApiException(String expMessage) {
        super(expMessage);
    }
}
