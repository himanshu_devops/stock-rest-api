package com.rest.stockapi.interfaces;

import com.rest.stockapi.models.Stock;

import java.util.List;


/**
 * Inteface for Stock API service
 * @Author Himanshu Tyagi
 */

public interface StockApiService {
    /**
     * To get the list of all the stocks from DB
     * @return list of stocks
     */

    public List<Stock> getAllStocks();

    /**
     * to fetch stock details based on stock id
     * @param stockId
     * @return object of Stock
     */

    public Stock getStockById(Long stockId);

    /**
     *  add a new stock in DB
     * @param stock
     * @return void
     */
    public void addStock(Stock stock);

    /**
     * Update stock based on stock id
     * @param stockId
     * @param stock
     * @return void
     */

    public void updateStock(Long stockId, Stock stock);
}
