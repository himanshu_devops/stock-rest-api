package com.rest.stockapi.service;

import com.rest.stockapi.exceptions.StockApiException;
import com.rest.stockapi.interfaces.StockApiService;
import com.rest.stockapi.models.Stock;
import com.rest.stockapi.repositories.StockRepository;
import com.rest.stockapi.utils.StockApiConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * Implementation class for Stock API service interface
 * @Author Himanshu Tyagi
 */

@Service
public class StockApiServiceImpl implements StockApiService {

    private static final Logger logger = LoggerFactory.getLogger(StockApiServiceImpl.class);

    @Autowired
    private StockRepository stockRepository;

    /**
     * To get the list of all the stocks from DB
     * @return list of stocks
     */

    @Override
    public List<Stock> getAllStocks()
    {
        List<Stock> stockList=new ArrayList<>();
        stockRepository.findAll().forEach(stockList::add);
        return stockList;
    }

    /**
     * to fetch stock details based on stock id
     * @param stockId
     * @return object of Stock
     */

    @Override
    public Stock getStockById(Long stockId)
    {
        Optional<Stock> stock=stockRepository.findById(stockId);
        if(stock.isPresent()) { return new Stock(stock.get()); }
        else {
            throw new StockApiException(String.format(StockApiConstants.STKAPI001, stockId));
        }
    }

    /**
     *  add a new stock in DB
     * @param stock
     * @return void
     */

    @Override
    public void addStock(Stock stock)
    {
       stockRepository.save(stock);
    }

    /**
     * Update stock based on stock id
     * @param stockId
     * @param stock
     * @return void
     */

    @Override
    public void updateStock(Long stockId, Stock stock)
    {
        Optional<Stock> stockExists=stockRepository.findById(stockId);
        if(stockExists.isPresent())
        {
            Stock stockToUpdate=stockExists.get();
            if(Objects.nonNull(stock)) {
                stockToUpdate.setStockName(stock.getStockName());
                stockToUpdate.setCurrentPrice(stock.getCurrentPrice());
                stockRepository.save(stockToUpdate);
                logger.info(StockApiConstants.UPDATED_DETAILS, stockToUpdate.getStockName(), stockToUpdate.getCurrentPrice());
            }
        }
        else
        {
            throw new StockApiException(String.format(StockApiConstants.STKAPI001, stockId));
        }


    }
}
