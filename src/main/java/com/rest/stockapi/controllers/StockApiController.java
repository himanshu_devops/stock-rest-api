package com.rest.stockapi.controllers;

import com.rest.stockapi.models.Stock;
import com.rest.stockapi.service.StockApiServiceImpl;
import com.rest.stockapi.utils.StockApiConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * REST Controller for Stock API
 * @Author Himanshu Tyagi
 */

@RestController
public class StockApiController {
    private static final Logger logger = LoggerFactory.getLogger(StockApiController.class);

    @Autowired
    private StockApiServiceImpl stockApiServiceImpl;

    /**
     * To get the list of all the stocks from DB
     * @return list of stocks
     */

    @GetMapping(path = "/stocks")
    public ResponseEntity<List<Stock>> getAllStocks()
    {
        logger.info(StockApiConstants.LIST_STOCK);
        return new ResponseEntity<List<Stock>>(stockApiServiceImpl.getAllStocks(), HttpStatus.OK);
    }

    /**
     * to fetch stock details based on stock id
     * @param stockId
     * @return object of Stock
     */

    @GetMapping(path = "/getStock/{stockId}")
    public ResponseEntity<Stock> getStockById(@PathVariable @NotNull Long stockId)
    {
        logger.info(StockApiConstants.STOCK_ID, stockId);
        return new ResponseEntity<Stock>(stockApiServiceImpl.getStockById(stockId), HttpStatus.OK);
    }

    /**
     *  add a new stock in DB
     * @param stock
     * @return void
     */

    @PostMapping(value="/addStock")
    public ResponseEntity<Stock> addStock(@RequestBody Stock stock)
    {
        logger.info(StockApiConstants.STOCK, stock);
        stockApiServiceImpl.addStock(stock);
        return new ResponseEntity<>(stock, HttpStatus.OK);

    }

    /**
     * Update stock based on stock id
     * @param stockId
     * @param stock
     * @return void
     */

    @PutMapping(value="/updateStock/{stockId}")
    public ResponseEntity<Stock> updateStock(@PathVariable @NotNull Long stockId, @RequestBody Stock stock)
    {
        logger.info(StockApiConstants.STOCK_AND_ID, stockId, stock.getStockName());
        stockApiServiceImpl.updateStock(stockId,stock);
        return new ResponseEntity<>(stock, HttpStatus.OK);
    }
}
