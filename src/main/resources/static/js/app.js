var app = angular.module('app', []);

app.controller('StockCtrl', [
    '$scope',
    'StockService',
    function($scope, StockService) {
        $scope.onloadFun = function() {
            $scope.getAllStocks();
            $scope.stateChanged = false;
        }

        $scope.getAllStocks = function() {
            StockService.getAllStocks().then(
                function success(response) {
                    $scope.stocks = response.data;
                    $scope.message = '';
                    $scope.errorCode = '';
                }, function error() {
                    $scope.message = '';
                    $scope.errorCode = 'Error getting stocks!';
                });
        }

         $scope.updateStock = function(stock) {
         console.log(stock.stockName);
          if (stock != null && stock.stockName && stock.currentPrice) {
                    StockService.updateStock(stock).then(
                        function success(response) {
                            $scope.updateStockSuccess = 'Stock updated!';
                            $scope.updateStockError = '';
                            $scope.errorCode = '';
                            for(var i=0; i<$scope.stocks.length; i++){
                                if(response.data.id=== $scope.stocks[i].stockId){
                                    $scope.stocks[i]=response.data;
                                }
                            }

                        }, function error(response) {
                            $scope.updateStockSuccess = '';
                            $scope.errorCode = response.data.errorCode;
                            $scope.updateStockError = response.data.message;
                        });
                    }
           }

         $scope.addStock = function(stock) {
                     $scope.stateChanged = true;
                     if (stock != null && stock.stockName && stock.currentPrice) {
                         StockService.addStock(stock).then(
                             function success() {
                                 $scope.stocks = $scope.getAllStocks();
                                 $scope.addStockSuccess = 'Stock added successfully!';
                                 $scope.addStockError = '';
                                 $scope.errorMessage = '';
                             }, function error(response) {
                                 $scope.addStockSuccess = '';
                                 $scope.errorCode = response.data.errorCode
                                 $scope.addStockError = response.data.message;
                             });
                     }
                 }
         $scope.getStock = function(stockId) {
                    $scope.shouldShowSearchTable = false;
                    if(!stockId) {
                        $scope.idEmpty = true;
                    } else {
                        $scope.idEmpty = false;
                        StockService.getStock(stockId).then(
                            function success(response) {
                                $scope.shouldShowSearchTable = true;
                                $scope.stock = response.data;
                                $scope.message = '';
                                $scope.errorCode = '';
                            }, function error(response) {
                                $scope.message = response.data.message;
                                $scope.errorCode = response.data.errorCode;

                            });
                    }
                }
    } ]);

app.service('StockService', [ '$http', function($http) {

    this.getAllStocks = function getAllStocks() {
        return $http({
            method : 'GET',
            url : 'stocks/'
        });
    }

    this.updateStock = function updateStock(stock) {
            return $http({
                method : 'PUT',
                url : 'updateStock/' + stock.stockId,
                data : {
                    stockId : stock.stockId,
                    stockName : stock.stockName,
                    currentPrice : stock.currentPrice
                }
            })
        }

    this.addStock = function addStock(stock) {
             return $http({
                method : 'POST',
                url : 'addStock/',
                data : {
                    stockId : stock.stockId,
                    stockName : stock.stockName,
                    currentPrice : stock.currentPrice
                 }
                });
            }

    this.getStock = function getStock(stockId) {
            return $http({
                method : 'GET',
                url : 'getStock/' + stockId
            });
        }

} ]);