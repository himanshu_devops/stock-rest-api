# STOCK-REST-API

**Stock-rest-api has been designed to handle stock related operation like:**

1. Adding a new stock

2. Fetching all the stocks from the database

3. Fetching a stock by stock id

4. Updating a stock

**Following are used for the development:**

1. IntelliJ IDEA 2018
2. Spring Boot
3. JDK 1.8
4. In memory H2 Database of Spring Boot
5. Tomcat server of Spring Boot
6. Apache maven 3.6
7. SCM - BitBucket
8. PostMan - to test REST endpoints

**Steps to checkout the code from BitBucket repository:**

**It can be done in multiple ways:**

1. BitBucket repository URL-  https://himanshu_devops@bitbucket.org/himanshu_devops/stock-rest-api.git  (Master branch)

Download and unzip the source in your local machine

or

Clone the repository using git, use command:

"git clone https://himanshu_devops@bitbucket.org/himanshu_devops/stock-rest-api.git"    (Master branch)

**Steps to start the application:**

Using command prompt or terminal

Go to the root folder of the project and use the maven Spring Boot plugin-  mvn spring-boot:run

OR

Import the application source code in any IDE and then run the main method of the application- StockRestApiApplication.java

**Steps to run the application**

User interface has been addded to the REST services which can be accessed via :

**URL : http://localhost:8082/api/stock-home.html**

Note: Server Port has been customized to 8082

As spring security is enabled to secure the REST endpoints, you'll be promted for username and password. Kindly use below details:

        Username : admin
        Password : admin

(Kindly refrain exposing credentials publically for higher environments)

Note: There are two srping boot profiles - 1. local and 2. prod, among these "local profile is active"


**Test REST endpoints using POSTMAN**

URLS & Authorization:

For all the operations in POSTMAN, kindly use :

Authorization:

         Type     : Basic Auth
         Username : admin
         Password : admin

**Get all the stocks**

        Method : GET
        URL    : http://localhost:8082/api/stocks

**Add a new stock**

        Method : POST
        URL    : http://localhost:8082/api/addStock
        Body   : {
                    "stockName":"CISCO",
                    "currentPrice":16.20
                 }

**Get a stock by ID**

        Method : GET
        URL    : http://localhost:8082/api/getStock/{stockId}

**Update a stock**

        Method : PUT
        URL    : http://localhost:8082/api/updateStock/{stockId}
        Body   : {
                     "stockName":"EuroMax"
                  }



